# shellcheck shell=bash
# shellcheck disable=SC2034
## Format YAML

PROG_NAME="ruamel"
ARGS_FILE_NAME=".ruamel"

function filter_files()
{
	grep -iE '\.ya?ml$'
}

function init()
{
	report_cmd cp "${FMTER_ROOT}/${MODE}/${FILETYPE}/${ARGS_FILE_NAME}" "$1"
	report_neutral "The ruamel config file format is a list of properties to set on the ruamel.yaml.YAML() object."
}

function run()
{
	local _config_file="$1"
	shift
	while read -r; do
		local _out=
		if [ "${MODE}" == "dump" ]; then
			_out="sys.stdout"
			echo "Dumping YAML: ${REPLY}..."
		else
			_out="open('${REPLY}', 'w')"
			echo -n "Formatting YAML: ${REPLY}..."
		fi
		python <<-EOF
			import sys
			from ruamel.yaml import YAML
			yaml = YAML(typ='rt')
			$(printf 'yaml.%s\n' "$@")
			doc = yaml.load(open("${REPLY}", 'r').read())
			yaml.dump(doc, ${_out})
		EOF
		# shellcheck disable=SC2181
		[ $? == 0 ] || echo -ne '\tFailed!'
		echo
		verbose echo "from ruamel.yaml import YAML with $(printf '"%s" ' "$@")"
	done
}
