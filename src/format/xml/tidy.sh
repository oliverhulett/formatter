# shellcheck shell=bash
# shellcheck disable=SC2034
## Format HTML/XML

PROG_NAME="tidy"
ARGS_FILE_NAME=".tidy"

function filter_files()
{
	grep -iE '\.(x|x?ht)ml$'
}

function init()
{
	report_cmd cp "${FMTER_ROOT}/${MODE}/${FILETYPE}/${ARGS_FILE_NAME}" "$1"
	report_neutral "The tidy config file format is a list of command line arguments to pass to tidy."
}

function run()
{
	local _config_file="$1"
	shift
	while read -r; do
		echo -n "Formatting XML/HTML:  ${REPLY}..."
		# shellcheck disable=SC2005
		echo "$(tidy -q "$@" "${REPLY}")" >"${REPLY}" || echo -ne '\tFailed!'
		echo
		verbose echo "tidy -q $* ${REPLY}"
	done
}
