# shellcheck shell=bash
# shellcheck disable=SC2034
## Format JSON

PROG_NAME="python_json_tool"

function filter_files()
{
	grep -iE '\.json$'
}

function init()
{
	report_neutral "No configuration for Python's json.tool..."
}

function run()
{
	local _config_file="$1"
	shift
	while read -r; do
		if [ "${MODE}" == "check" ]; then
			echo -n "Checking JSON: ${REPLY}..."
			diff -Nq "${REPLY}" <(python -m json.tool "${REPLY}") >/dev/null || echo -ne '\tFailed!'
		elif [ "${MODE}" == "dump" ]; then
			echo "Dumping JSON: ${REPLY}..."
			python -m json.tool "${REPLY}" || echo -ne '\tFailed!'
		elif [ "${MODE}" == "format" ]; then
			echo -n "Formatting JSON: ${REPLY}..."
			# shellcheck disable=SC2005
			echo "$(python -m json.tool "${REPLY}")" >"${REPLY}" || echo -ne '\tFailed!'
		else
			echo -e 'python_json_tool is not a valid reporting tool...\tFailed!'
		fi
		echo
		verbose echo "python -m json.tool ${REPLY}"
	done
}
