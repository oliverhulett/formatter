# shellcheck shell=bash
# shellcheck disable=SC2034
## Format Python
## See: https://github.com/ambv/black
## Disable formatting for sections with:
## # fmt: off
## FOO = {
##     # ... some very large, complex data literal.
## }
## # fmt: on

PROG_NAME="black"
ARGS_FILE_NAME=".black"

function filter_files()
{
	grep -iE '\.py$'
}

function init()
{
	report_cmd cp "${FMTER_ROOT}/${MODE}/${FILETYPE}/${ARGS_FILE_NAME}" "$1"
}

function run()
{
	local _config_file="$1"
	shift
	local _mode_flag=
	local _mode_string=
	if [ "${MODE}" == "check" ]; then
		_mode_flag="--check"
		_mode_string="Checking"
	elif [ "${MODE}" == "report" ]; then
		_mode_flag="--diff"
		_mode_string="Reporting on"
	else
		_mode_string="Formatting"
	fi
	while read -r; do
		echo -n "${_mode_string} Python: ${REPLY}..."
		black ${_mode_flag} "$@" "${REPLY}" || echo -ne '\tFailed!'
		echo
		verbose echo "black $* ${REPLY}"
	done
}
