# shellcheck shell=bash
# shellcheck disable=SC2034
## Format Python
## See: https://github.com/google/yapf
## Disable formatting for sections with:
## # yapf: disable
## FOO = {
##     # ... some very large, complex data literal.
## }
## # yapf: enable
## You can also disable formatting for a single literal like this:
## BAZ = {
##     # ... some very large, complex data literal.
## }  # yapf: disable

PROG_NAME="yapf"
ARGS_FILE_NAME=".yapf"
CONFIG_FILE_NAME=".style.yapf"

function filter_files()
{
	grep -iE '\.py$'
}

function init()
{
	report_cmd cp "${FMTER_ROOT}/${MODE}/${FILETYPE}/${CONFIG_FILE_NAME}" "$1"
	report_neutral "You can also override the yapf command line arguments by creating a ${ARGS_FILE_NAME} file in $1."
}

function run()
{
	local _config_file="$1"
	shift
	while read -r; do
		echo -n "Formatting Python: ${REPLY}..."
		yapf --in-place --style="${_config_file}" "${REPLY}" || echo -ne '\tFailed!'
		echo
		verbose echo "yapf --in-place --style=${_config_file} ${REPLY}"
	done
}
