# shellcheck shell=bash
# shellcheck disable=SC2034
## Format Shell
## See:  https://github.com/bemeurer/beautysh
## Disable formatting for sections with:
## # @formatter:off
## command \
##     --option1 \
##         --option2 \
##             --option3 \
## # @formatter:on

	PROG_NAME="beautysh"
ARGS_FILE_NAME=".beautysh"

function filter_files()
{
	grep -iE '\.(ba)?sh$|\.bats$'
}

function init()
{
	report_cmd cp "${FMTER_ROOT}/${MODE}/${FILETYPE}/${ARGS_FILE_NAME}" "$1"
	report_neutral "The beautysh config file format is a list of command line arguments to pass to beautysh."
}

function run()
{
	local _config_file="$1"
	shift
	while read -r; do
		echo -n "Formatting Shell: ${REPLY}..."
		beautysh "$@" --files "${REPLY}" || echo -ne '\tFailed!'
		echo
		verbose echo "beautysh $* --files ${REPLY}"
	done
}
