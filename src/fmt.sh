#!/bin/bash
##
##	Format files in this project.  It saves on arguments.
##	This is the entrypoint, there are three main options, 'format', 'report', and 'check'.
##	Install dependencies in the docker container, so here we can assume we have whatever we need installed,
##	our job is just to marshal arguments and orchestrate the underlying programs and their outputs.
##
# shellcheck disable=SC1090

PROJECT_NAME="formatter"
FMTER_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"
source "${FMTER_ROOT}/utils.sh"

function print_help()
{
	echo "${PROJECT_NAME} [-v | --verbose] [-i | --init] [-l | --list-method=<METHOD>] format|check|report [-I | --include=<DIR>] [-E | --exclude=<DIR>] <linter_overrides> <dirs...>"
	echo "    -v --verbose:      Be verbose."
	echo "    -i --init:         Initialise a repo for use by writing default configuration files for the underlying linters."
	echo "    -l --list-method:  Specify 'find' to use \`find' or 'git' to use \`git ls-files' to discover files to process."
	echo "    -I --include:      Include only these directories when discovering files to process."
	echo "    -E --exclude:      Exclude these directorieas when discovering files to process."
	echo
	echo "  format | check | report | dump:  The mode in which to run."
	echo "    format:  Overwrite files with formatted output."
	echo "    check:   Check files for formatting errors and set the program exit code to the number of linting errors.  This can be used to ratchet and fail builds."
	echo "    report:  Report linting errors and statistics to a file."
	echo "    dump:    Dump formatted versions of all the files to stdout."
	echo
	echo "  linter_overrides"
	echo "    Pass command line overrides to specific linters by creating files containing the args.  One arg per line."
	echo "    Specify the override files on the ${PROJECT_NAME} command line with --<linter_program_name>=<override_file>."
	echo "    Leave the <override_file> argument blank to use the default command line for a non-default linter."
	echo
	echo "  dirs..."
	echo "    The directories in which to discover files to process."
	echo "    The linters will be run from each of these directories, so they can each have their own linter configuration files."
	echo "    --include and --exclude directory patterns are interpreted relative to each of these directories."
}

VERBOSE="false"
INIT="false"
LIST_METHOD="git"
declare -a INCLUDE
declare -a EXCLUDE
MODE=
declare -a DIRS
declare -A OVERRIDES

OPTS=$(getopt -o "hvil:I:E:" --long "help,verbose,init,list-method:,include:,exclude:" -n "${PROJECT_NAME}" -- "$@")
es=$?
if [ $es != 0 ]; then
	print_help
	# Can't use exit code from getopt because the user may be using our exit code to count linting failures.
	exit -1
fi
eval set -- "${OPTS}"
while true; do
	case "$1" in
		-h | '-?' | --help )
			print_help
			exit 0
			;;
		-v | --verbose )
			VERBOSE="true"
			shift
			;;
		-i | --init )
			INIT="true"
			shift
			;;
		-l | --list-method )
			case "$2" in
				git | find )
					LIST_METHOD="$2"
					;;
				* )
					report_bad "Bad list method.  Specify one of 'git', or 'find'"
					print_help
					exit -1
					;;
			esac
			shift 2
			;;
		-I | --include )
			INCLUDE[${#INCLUDE[@]}]="$2"
			shift 2
			;;
		-E | --exclude )
			EXCLUDE[${#EXCLUDE[@]}]="$2"
			shift 2
			;;
		-- )
			shift
			;;
		format | check | report | dump )
			MODE="$1"
			shift
			;;
		--* )
			p="$(echo "$1" | sed -nre 's/^--([^=]+)=?.*$/\1/p')"
			if echo "$1" | grep -q =; then
				o="$(echo "$1" | sed -nre 's/^.+=(.+)$/\1/p')"
				shift
			elif [ -z "$2" ] || [ "${2#-}" != "$2" ]; then
				o=
				shift
			else
				o="$2"
				shift 2
			fi
			OVERRIDES["$p"]="$o"
			;;
		* )
			DIRS[${#DIRS[@]}]="$1"
			shift
			;;
	esac
	if [ $# == 0 ]; then
		break
	fi
done

function verbose()
{
	if [ "${VERBOSE}" == "true" ]; then
		"$@"
	fi
}

if [ -z "${MODE}" ]; then
	report_bad "No mode.  Specify one of 'format', 'check', 'report', or 'dump'"
	print_help
	exit -1
fi

if [ ${#DIRS[@]} == 0 ]; then
	verbose report_neutral "No directories specified, using CWD:  $(pwd)"
	DIRS[${#DIRS[@]}]="$(pwd)"
fi

declare -g _LAST_ARGS
function _recall()
{
	if [ "${_LAST_ARGS}" == "$*" ]; then
		return 0
	else
		_LAST_ARGS="$*"
		return 1
	fi
}

function _git_lsfiles()
{
	local _dir="$1"
	shift
	if [ -d "$_dir" ]; then
		cd "$_dir" && git ls-files "$@"
	else
		echo "$_dir"
	fi
}
function _find_lsfiles()
{
	local _dir="$1"
	shift
	if [ -d "$_dir" ]; then
		cd "$_dir" && find . -xdev -not \( -name '.git' -prune -or -name '.svn' -prune -or -name '.venv' -prune -or -name '.virtualenv' -prune -or -name 'node_modules' -prune \) -type f "$@"
	else
		echo "$_dir"
	fi
}
function _exclude()
{
	if [ $# -gt 0 ]; then
		local _first="$1"
		shift
		grep -vE "$_first$(printf '%s' "${@/#/|}")"
	else
		cat -
	fi
}
function _include()
{
	if [ $# -gt 0 ]; then
		local _first="$1"
		shift
		grep -E "$_first$(printf '%s' "${@/#/|}")"
	else
		cat -
	fi
}

declare -ga FILES
function lsfiles()
{
	if ! _recall "$@"; then
		local _ORIG_LIST_METHOD="${LIST_METHOD}"
		if [ "${LIST_METHOD}" == "git" ]; then
			readarray -t FILES <<<"$(_git_lsfiles "$@" 2>/dev/null | _exclude "${EXCLUDE[@]}" | _include "${INCLUDE[@]}")"
			if [ ${#FILES[@]} -eq 0 ] || [ -z "${FILES[0]}" ]; then
				LIST_METHOD="find"
			fi
		fi
		if [ "${LIST_METHOD}" == "find" ]; then
			readarray -t FILES <<<"$(_find_lsfiles "$@" 2>/dev/null | _exclude "${EXCLUDE[@]}" | _include "${INCLUDE[@]}")"
		fi
		LIST_METHOD="${_ORIG_LIST_METHOD}"
	fi
	printf '%s\n' "${FILES[@]}"
}

if [ "${INIT}" == "true" ]; then
	report_good "Initialising ${MODE} mode for directories: ${DIRS[*]}"
else
	report_good "Running ${MODE} mode for directories: ${DIRS[*]}"
fi

function _run_for_files()
{
	local _dir="$1"
	shift
	local -a _args
	if test "${OVERRIDES[${PROG_NAME}]+isset}" && [ -n "${OVERRIDES[${PROG_NAME}]}" ]; then
		readarray -t _args <"${OVERRIDES[${PROG_NAME}]}"
	elif test "${ARGS_FILE_NAME+isset}" && [ -r "$_dir/${ARGS_FILE_NAME}" ]; then
		readarray -t _args <"$_dir/${ARGS_FILE_NAME}"
	elif test "${ARGS_FILE_NAME+isset}" && [ -f "$_dir" ] && [ -r "$(dirname "$_dir")/${ARGS_FILE_NAME}" ]; then
		readarray -t _args <"$(dirname "$_dir")/${ARGS_FILE_NAME}"
	elif test "${ARGS_FILE_NAME+isset}" && [ -r "${FMTER_ROOT}/${MODE}/$FILETYPE/${ARGS_FILE_NAME}" ]; then
		readarray -t _args <"${FMTER_ROOT}/${MODE}/${FILETYPE}/${ARGS_FILE_NAME}"
	fi
	local _config_file
	if test "${CONFIG_FILE_NAME+isset}" && [ -r "$_dir/${CONFIG_FILE_NAME}" ]; then
		_config_file="$_dir/${CONFIG_FILE_NAME}"
	elif test "${CONFIG_FILE_NAME+isset}" && [ -f "$_dir" ] && [ -r "$(dirname "$_dir")/${CONFIG_FILE_NAME}" ]; then
		_config_file="$(dirname "$_dir")/${CONFIG_FILE_NAME}"
	elif test "${CONFIG_FILE_NAME+isset}" && [ -r "${FMTER_ROOT}/${MODE}/${FILETYPE}/${CONFIG_FILE_NAME}" ]; then
		_config_file="${FMTER_ROOT}/${MODE}/${FILETYPE}/${CONFIG_FILE_NAME}"
	fi
	local -a _files
	readarray -t _files <<<"$(lsfiles "${_dir}" | filter_files)"
	if [ ${#_files[@]} -gt 0 ] && [ -n "${_files[0]}" ]; then
		report_good "Running ${PROG_NAME} for ${FILETYPE} files in ${MODE} mode in ${_dir}..."
		printf '%s\n' "${_files[@]}" | run "${_config_file}" "${_args[@]}"
	fi
}

function _init_for_files()
{
	local _dir="$1"
	shift
	local -a _files
	readarray -t _files <<<"$(lsfiles "${_dir}" | filter_files)"
	if [ ${#_files[@]} -gt 0 ] && [ -n "${_files[0]}" ]; then
		report_good "Initialising ${PROG_NAME} for ${FILETYPE} files in ${MODE} mode in ${_dir}..."
		printf '%s\n' "${_files[@]}" | init "${_dir}"
	fi
}

trap 'echo; report_good Done' EXIT

shopt -s nullglob
for d in "${DIRS[@]}"; do
	for t in "${FMTER_ROOT}/${MODE}"/*; do
		FILETYPE="$(basename -- "$t")"
		_fmter="$t/default.sh"
		for p in "${!OVERRIDES[@]}"; do
			if [ -r "$t/$p.sh" ]; then
				_fmter="$t/$p.sh"
				break
			fi
		done
		unset PROG_NAME ARGS_FILE_NAME CONFIG_FILE_NAME
		unset -f filter_files init run

		if [ -r "${_fmter}" ]; then
			source "$_fmter"
			if [ "${INIT}" == "true" ]; then
				_init_for_files "$d"
			else
				_run_for_files "$d"
			fi
		else
			report_bad "No handler found for ${FILETYPE} in ${MODE} mode:  ${_fmter}"
		fi
	done
done
