# shellcheck shell=bash
##
##	This file is intended to be sourced.  It will setup a development environment for this project.
##	Once this file "returns" you will be in a python virtual environment with this project installed
##	in develop mode and all the testing and development dependencies installed.
##

HERE="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"
EXE="$(cd "$(dirname -- "$0")" && pwd -P)/$(basename -- "$0")"
VENV_DIR="${HERE}/.venv"
REQUIREMENTS="${HERE}/requirements.txt"


GREEN="$(tput -T"${TERM:-dumb}" setaf 2 || true)"
WHITE="$(tput -T"${TERM:-dumb}" bold || true)$(tput -T"${TERM:-dumb}" setaf 7 || true)"
RESET="$(tput -T"${TERM:-dumb}" sgr0 || true)"
function _tell()
{
	echo "${WHITE} $* ${RESET}"
}
function _success()
{
	echo "${GREEN} $* ${RESET}"
}

_CHANGED="false"

if [ ! -e "${VENV_DIR}/bin/activate" ]; then
	_tell "Creating python virtual environment: ${VENV_DIR}"
	_CHANGED="true"
	virtualenv -p "$(command which python2.7)" --prompt="(.venv:fmter) " "${VENV_DIR}" || return 1
fi

if [ "${VIRTUAL_ENV}" != "${VENV_DIR}" ]; then
	if [ "${EXE}" != "${HERE}/inv" ]; then
		# Don't advertise if we've been sourced from ./inv, you can't take it with you.
		_tell "Activating python virtual environment: ${VENV_DIR}"
		_CHANGED="true"
	fi
	# shellcheck source=.venv/bin/activate
	# shellcheck disable=SC1091
	source "${VENV_DIR}/bin/activate"
fi

if [ ! -e "${VENV_DIR}/requirements.txt" ] || [ "${REQUIREMENTS}" -nt "${VENV_DIR}/requirements.txt" ]; then
	_tell "Installing python development requirements: requirements.txt"
	_CHANGED="true"
	cp "${REQUIREMENTS}" "${VENV_DIR}/requirements.txt"
	pip install -q -r "${VENV_DIR}/requirements.txt"
fi

if [ "${_CHANGED}" == "true" ]; then
	# Don't advertise if we haven't changed anything, the dev env was already set up.
	_success "Formatter development environment set up"
fi
