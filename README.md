# Formatter

A docker image to format all-the-things.

This project is intended to build a docker image that runs against a source tree.

```
$ docker run -t --rm -u "$(id -u)":"$(id -g)" -w "$(pwd)" -v "$(pwd)":"$(pwd)" oliverhulett/formatter
```

Of course, in order for the docker container to have access to you source tree, it needs to be mapped as a volume.  The above example will work for the CWD but will need to be modified for other use cases.  For convenience, `run.sh` exists in the root of this repository that wraps the docker run command and maps through the directories specified on the command line.

```
$ bash <(curl -sS https://bitbucket.org/oliverhulett/formatter/raw/master/run.sh) --help
```

Or download an commit a copy of `run.sh` into your repository.

The docker repository and image tag launched by `run.sh` can be specified by setting `$REPO` and `$TAG` environment variables.


## Usage

Run the docker image against a folder and it will format everything it can.  All of the heavy lifting is done by underlying linters, which can be configured with their built-in configuration files or with an arguments file that contains the flags that will be added to the linter's command line.

```
fmt.sh [-v | --verbose] [-n | --not-git] [-i | --init] format|check|report [-I | --include=<DIR>] [-E | --exclude=<DIR>] <linter_overrides> <dirs...>
    -v --verbose:  Be verbose.
    -n --not-git:  Use `find' instead of `git ls-files' to discover files to process.
    -i --init:     Initialise a repo for use by writing default configuration files for the underlying linters.
    -I --include:  Include only these directories when discovering files to process.
    -E --exclude:  Exclude these directorieas when discovering files to process.

  format | check | report:  The mode in which to run.
    format:  Overwrite files with formatted output.
    check:   Check files for formatting errors and set the program exit code to the number of linting errors.  This can be used to ratchet and fail builds.
    report:  Report linting errors and statistics to a file.

  linter_overrides
    Pass command line overrides to specific linters by creating files containing the args.
    Specify the override files on the fmt.sh command line with --<linter_program_name>=<override_file>.
    Leave the <override_file> argument blank to use the default command line for a non-default linter.

  dirs...
    The directories in which to discover files to process.
    The linters will be run from each of these directories, so they can each have their own linter configuration files.
    --include and --exclude directory patterns are interpreted relative to each of these directories.
```


## Why

It stops arguments...


## Contributing

* Set up your development environment by sourcing `init.sh`.  (It installs invoke, so you can skip this if you already have pyinvoke installed.)
* Build and run tests with `invoke`.
    * See `inv -h` and `inv -l` to get started.
    * Try `inv all` to fully check your commit.
    * Try `inv tests` to run all tests.
* Please format code with `inv format` before committing (it saves arguments over style.)
* Create a new version of the docker image with `inv docker.build` and push it with `inv docker.push`.
* Use the BitBucket Wiki for documentation.
* Use BitBucket Issues to track issues, create feature requests, and discuss proposals and changes.

### Adding a new tool

1. Add a handler in the appropriate sub-folder of `src/format`, `src/check`, or `src/report` for the language your new handler parses.
1. Describe the tool with:
    * `PROG_NAME`:  The name of the tool.  This will be used by the `linter_overrides` and for display.
    * `ARGS_FILE_NAME`:  The filename of the tool's command line argument file.  Only set this if there is one, the user can override with the `linter_overrides`.
    * `CONFIG_FILE_NAME`:  The filename of the tool's configuration file.
1. Implement the handler by implementing the functions:
    * `filter_files()`:  Should filter a stream of filenames, keeping only those that are processed by this tool.
    * `init()`:  Should initialise the directory given as `$1` with the default configuration for this tool.
    * `run()`:  Should run the tool on the stream of filesnames that will be presented on `stdin`.  You probably want to execute your tool in a `while read -r` loop.
        * The first argument to the `run()` function is the configuration file to use, it may be blank or it may have been provided by the directory to be formated, so don't assume you're using your default configuration file.
        * Subsequent arguments to the `run()` function come from the `ARGS_FILE_NAME` file and should be passed to the tool's command line.
1. Write a [bats](https://github.com/sstephenson/bats) test for your handler.
1. Install it in the `Dockerfile`
