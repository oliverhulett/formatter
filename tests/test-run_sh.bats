# shellcheck shell=bash
# shellcheck disable=SC1090

PROJECT_TESTS="$(cd "${BATS_TEST_DIRNAME}" && pwd -P)"
PROJECT_ROOT="$(dirname "${PROJECT_TESTS}")"
source "${PROJECT_TESTS}/utils.sh"


@test "run.sh calls docker" {
	stub docker "run -t --rm -u $(id -u):$(id -g) -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):$(pwd) -w $(pwd) oliverhulett/formatter:latest"
	run bash "${PROJECT_ROOT}/run.sh"
	unstub docker
}

@test "run.sh honours REPO and TAG environment variables" {
	stub docker "run -t --rm -u $(id -u):$(id -g) -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):$(pwd) -w $(pwd) http://docker.repo/oliverhulett/formatter:tag"
	REPO="http://docker.repo/" TAG="tag" run bash "${PROJECT_ROOT}/run.sh"
	unstub docker
}

@test "run.sh maps command line folders into the docker container" {
	D="$(cd "$(mktemp -d)" && pwd -P)"
	pushd "$D" || fail "Failed to change into temp dir"
	mkdir --parents dir/one two
	stub docker "run -t --rm -u $(id -u):$(id -g) -v /var/run/docker.sock:/var/run/docker.sock -v $D/dir/one:$D/dir/one -v $(pwd)/two:$(pwd)/two -v $(pwd):$(pwd) -w $(pwd) oliverhulett/formatter:latest -v $D/dir/one check ./two"
	run bash "${PROJECT_ROOT}/run.sh" -v "$D/dir/one" check ./two
	unstub docker
	popd || fail "Failed to change out of temp dir"
	rm -rf "$D"
}
