# shellcheck shell=bash
# shellcheck disable=SC1090

## Load bats libraries
PROJECT_TESTS="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"
PROJECT_ROOT="$(dirname "${PROJECT_TESTS}")"
export PROJECT_TESTS PROJECT_ROOT
source "${PROJECT_TESTS}/x_helpers/bats-support/load.bash"
source "${PROJECT_TESTS}/x_helpers/bats-assert/load.bash"
source "${PROJECT_TESTS}/x_helpers/bats-file/load.bash"
source "${PROJECT_TESTS}/x_helpers/bats-mock/load.bash"
