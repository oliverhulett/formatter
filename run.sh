#!/bin/bash

VOLUMES=()
for i in "$@"; do
	if [ -d "$i" ]; then
		_dir="$(cd "$i" && pwd -P)"
		VOLUMES[${#VOLUMES[@]}]="-v"
		VOLUMES[${#VOLUMES[@]}]="${_dir}:${_dir}"
	fi
done

for cmd in echo ""; do
	$cmd docker run -t --rm -u "$(id -u)":"$(id -g)" -v /var/run/docker.sock:/var/run/docker.sock "${VOLUMES[@]}" -v "$(pwd)":"$(pwd)" -w "$(pwd)" "${REPO}oliverhulett/formatter:${TAG:-latest}" "$@"
done
