import os

from invoke import task, call, Collection

_PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
_IMAGE_NAME = "oliverhulett/formatter"
_dev_image_tag = "dev"
_src_dirs = ["src"]
_test_dirs = ["tests"]

docker = Collection("docker")
ns = Collection(docker)


@task
def build(ctx, image_tag="latest"):
    with ctx.cd(_PROJECT_DIR):
        ctx.run("docker build --tag={}:{} .".format(_IMAGE_NAME, image_tag))


docker.add_task(build)


@task
def push(ctx, image_tag="latest"):
    ctx.run("docker push {}:{}".format(_IMAGE_NAME, image_tag))


docker.add_task(push)


@task
def format(ctx, verbose=False, image_tag=_dev_image_tag):
    with ctx.cd(_PROJECT_DIR):
        ctx.run("TAG={} {pd}/run.sh format {} {pd}".format(image_tag, "-v" if verbose else "", pd=_PROJECT_DIR))


ns.add_task(format)


@task
def check(ctx, verbose=False, image_tag=_dev_image_tag):
    with ctx.cd(_PROJECT_DIR):
        ctx.run("TAG={} {pd}/run.sh check {} {pd}".format(image_tag, "-v" if verbose else "", pd=_PROJECT_DIR))


ns.add_task(check)


@task
def report(ctx, verbose=False, image_tag=_dev_image_tag):
    with ctx.cd(_PROJECT_DIR):
        ctx.run("TAG={} {pd}/run.sh report {} {pd}".format(image_tag, "-v" if verbose else "", pd=_PROJECT_DIR))


ns.add_task(report)


@task
def dump(ctx, verbose=False, image_tag=_dev_image_tag):
    with ctx.cd(_PROJECT_DIR):
        ctx.run("TAG={} {pd}/run.sh dump {} {pd}".format(image_tag, "-v" if verbose else "", pd=_PROJECT_DIR))


ns.add_task(report)


@task(aliases=("tests",))
def test(ctx):
    ctx.run("{}/tests/run.sh".format(_PROJECT_DIR))


ns.add_task(test)


@task
def all(ctx, verbose=False):
    docker["build"](ctx, _dev_image_tag)
    ns["test"](ctx)
    ns["format"](ctx, verbose)
    ns["check"](ctx, verbose)
    ns["report"](ctx, verbose)
    ns["test"](ctx)
    docker["build"](ctx)


ns.add_task(all)


@task
def clean(ctx, _all=False, _docker=False):
    if _all or _docker:
        docker["clean"](ctx)
    with ctx.cd(_PROJECT_DIR):
        for d in (".venv",):
            ctx.run('rm -rf "{}"'.format(d), warn=True)


ns.add_task(clean)
