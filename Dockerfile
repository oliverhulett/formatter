FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
	git \
	python-pip \
	python3-pip \
	tidy \
;

RUN pip install \
	git+https://github.com/oliverhulett/beautysh.git@oliver/support-here-strings \
	https://sourceforge.net/projects/pychecker/files/pychecker/0.8.19/pychecker-0.8.19.tar.gz/download \
	ruamel.yaml \
	yapf \
;

RUN pip3 install \
	black \
;

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY ./src /fmter

ENTRYPOINT ["/fmter/fmt.sh"]
